using System;
using System.Collections.Generic;
using System.Linq;
using RazorPages.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using RazorPages.Data;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace RazorPages.Pages.Student
{ 
    public class IndexModel : PageModel
    {
        private readonly RazorPagesDbContext _context;
       
        public IndexModel(RazorPagesDbContext context)
        {
            _context = context;
        }
        
        public IList<Models.Student> Students;
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
        [BindProperty(SupportsGet = true)]
        public double SearchDoubleGreates { get; set; }
        [BindProperty(SupportsGet = true)]
        public double SearchDouble { get; set; }
        [BindProperty(SupportsGet = true)]
        public double SearchDoubleLess { get; set; }

        public async Task OnGetAsync()
        {
            Students = await _context.Students.ToListAsync();
            var students = from m in _context.Students
                         select m;
            if (!string.IsNullOrEmpty(SearchString))
            {
                students = students.Where(s => s.LastName.Contains(SearchString)); // ���������� Linq ������ ���������� � �� ��������� ���� SQL 
            }
// ������ ��� ���� ��� ������ �������� � �������� ��� ��� �� ���� ���������� � ���� ��������� ������ � ����� �������� ������� �������� �������� � ������� 
            if (!(SearchDoubleGreates == 0.0))
            {
                students = students.Where(p => p.GPA >= SearchDoubleGreates);
            }
            if (!(SearchDoubleLess == 0.0))
            {
                students = students.Where(p => p.GPA <= SearchDoubleLess);
            }

            Students = await students.ToListAsync();
        }
    }
}